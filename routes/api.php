<?php
// namespace App\Http\Controllers\API\AdminController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\UserController;
use App\Http\Controllers\API\AdminController;
use App\Http\Controllers\API\OrderController;
//use App\Models\order;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//User Routes

Route::post('login', [UserController::class,'login']);
Route::post('register',[UserController::class,'register']);
Route::group(['middleware' => 'auth:api'], function(){
Route::post('details', [UserController::class, 'details']);
//order
Route::get('order', [OrderController::class,'show']);
Route::post('logout', [UserController::class,'logout']);
});
